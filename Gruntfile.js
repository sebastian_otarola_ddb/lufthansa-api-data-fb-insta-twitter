module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      options: {
        livereload: 1337
      },
      base: {
        files: ['**/*.html', '**/*.css']
      },
      css: {
        files: ['**/*.less'],
        tasks: ['less']
      }
    },

    less: {
      development: {
        files: {
          "style.css": "style.less"
        }
      }
    }


  });

  //Load the plugins
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');

  //Default task
  grunt.registerTask('default', ['watch']);

};