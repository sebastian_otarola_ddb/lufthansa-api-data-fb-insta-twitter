/* global $, document, window, FB */
$(document).ready(function(){

 function statusChangeCallback(response) {
  console.log(response);
  if (response.status === 'connected') {
   testAPI();
  } else if (response.status === 'not_authorized') {
   document.getElementById('status').innerHTML = 'Please log ' +
    'into this app.';
  } else {
   document.getElementById('status').innerHTML = 'Please log ' +
    'into Facebook.';
  }
 }

 function checkLoginState() {
  FB.getLoginStatus(function(response) {
   statusChangeCallback(response);
  });
 }

 window.fbAsyncInit = function() {
  FB.init({
   appId      : '1599745873571013',
   cookie     : true,  // enable cookies to allow the server to access 
   // the session
   xfbml      : true,
   version    : 'v2.2'
  });

  FB.getLoginStatus(function(response) {
   statusChangeCallback(response);
  });

 };

 // Load the SDK asynchronously
 (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

 // Run a simple test of Graph API whenr login is successful.

 function testAPI() {
  FB.api('/me', function(response) {
   console.log('%c Successful login for: ' + response.name, 'color:green');
  });
 }



 dataFromFacebook = {
  messageData: function() {
   FB.api('/me/posts', function(response) {
    var data = response.data;
    for (var key in data) {
     if(data[key].message) {
      console.log(data[key].message);
     }
    }
   });
  },
  checkSmileys: function(){
   FB.api('/me/posts', function(response) {
    var data = response.data;
    for (var key in data) {
     if(data[key].message) {
      checkStringValue(data[key].message);
     }
    }
   });
  }
 };


 $('.check-data').on('click', function(){
  dataFromFacebook.messageData()
 });
 
 var smileyFaces = [
  ':)', //ASCII smiley
  '\ud83d\ude31', //Ghost Face
  '<3' //ASCII heart
 ];
 
 function checkStringValue(text){
  for (var smiley in smileyFaces) {
   if(text.includes(smileyFaces[smiley])) {
    console.log(text);
   }
  }
 }


 $('.smileys').on('click', function(){
  console.log('\n\n');
  dataFromFacebook.checkSmileys()
 });

});