/* global $, document */
$(document).ready(function(){


  function readData(){
    $.ajax({
      type: 'GET',
      url: 'twitter-api/tweets_json.php',
      dataType: 'json',
      success: function(data) {
        for (var item in data) {
          console.log(data[item].text);
        }
      },
      error: function(e) {
        console.log('Error ' + JSON.stringify(e));
      }
    });
  }

  function checkData(){
    $.ajax({
      type: 'GET',
      url: 'twitter-api/tweets_json.php',
      dataType: 'json',
      success: function(data) {
        for (var item in data) {
          checkStringValue(data[item].text);
        }
      },
      error: function(e) {
        console.log('Error ' + JSON.stringify(e));
      }
    });
  }
  
  var smileyFaces = [
    ':)', //ASCII smiley
    '\ud83d\ude31', //Ghost Face
    '<3', //ASCII heart
    ':D'
  ];
  
  function checkStringValue(text){
    for (var smiley in smileyFaces) {
      if(text.includes(smileyFaces[smiley])) {
        console.log('%c ' + text, 'color: #26c319');
      } else {
        console.log('%c '+ text, 'color: #a03030');
      }
    }
  }

  function processTweets(){
    console.log('callback executed');
  }



  $('.check-data-twitter').on('click', function(){
    console.log('%c twitter button clicked', 'color: yellow');
    readData();
  });
  
  $('.smileys-twitter').on('click', function(){
    console.log('%c checking smiley availability \n\n', 'color: yellow');
    checkData();
  })

});