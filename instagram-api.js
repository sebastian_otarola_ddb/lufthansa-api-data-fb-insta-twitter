/* global $, document, window */
$(document).ready(function(){

 var client_info = {
  client_id: 'f6ec66b4a463413b8438d1bbd4f9db7c',
  client_secret: 'eebb4bd1b7434dabbc402ff4ad836e4f',
  website_url: 'http://localhost:8888/API-data/API-data/',
  redirect_uri: 'http://localhost:8888/API-data/API-data/',
  userID: 209696117,
  accessToken: '209696117.f6ec66b.a685d4849cd648a1b1a24fbb4fd4061d'
 };

 var url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' + client_info.accessToken;

 var loginUrl = 'https://api.instagram.com/oauth/authorize/?client_id='+ client_info.client_id + '&redirect_uri='+ client_info.redirect_uri + '&response_type=token'; 
 
 //Read Instagram feed with access token credentials
 function readData(){
  $.ajax({
   type: "GET",
   url: url,
   dataType: 'jsonp',
   success: function(data) {
    var data = data.data;
    for( var caption in data ) {
     console.log(data[caption].caption.text);
    }
   },
   error: function(e) {
    console.log(e);
   }
  });
 }

 //check instagram data
 function checkData(){
  $.ajax({
   type: "GET",
   url: url,
   dataType: 'jsonp',
   success: function(data) {
    var data = data.data;
    for( var caption in data ) {
     checkStringValue(data[caption].caption.text);
    }
   },
   error: function(e) {
    console.log(e);
   }
  });
 } 

 
/*Arraylist*/ 
 var smileyFaces = [
  ':)', //ASCII smiley
  '\ud83d\ude31', //Ghost Face
  '<3' //ASCII heart
 ];

/*
Check string from instagram
*/
 
 function checkStringValue(text){
  for (var smiley in smileyFaces) {
   if(text.includes(smileyFaces[smiley])) {
    console.log('%c ' + text, 'background:red;color:white');
   } 
  }
 }
 
 
//initiate instagram login authorization 
 $('.login-insta').on('click', function(){
  window.location.replace(loginUrl);
 });

 
 //Check instagram data once logged in to insta-application
 $('.check-data-insta').on('click', function(){
  console.log('button clicked');
  readData();
 });
 
 $('.smileys-insta').on('click', function(){
  checkData();
 })
 
 /*EOF*/
});